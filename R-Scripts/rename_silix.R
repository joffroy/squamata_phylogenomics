###rename all sequences in silix outputs

temp = list.files(pattern="*.fnodes")


Personalized_Reader <- function(lambda){
  read.delim(lambda, header = FALSE)}

myfiles <- lapply(temp, Personalized_Reader)



names<- read.delim("speciesnames_refseqs_final.txt", header=TRUE)
ord<-names$Accession

##because accessions are always in the same order in the dataframes try to order the names table instead
library("dplyr")
v<- myfiles[[15]][["V2"]]
nam_ordered<- left_join(data.frame(Accession=v),names, by="Accession")

nam<-nam_ordered$Species_name


newframes<-lapply(seq_along(myfiles), function(x) cbind(nam, myfiles[[x]]))

colnames=c("Species_name", "Family", "Accession")

final_frames<-lapply(newframes, setNames, colnames)

nams<- c(1:81)
names(final_frames)<- nams

mapply(
  write.table,
  x=final_frames, file=paste(names(final_frames), "txt", sep="."),
  MoreArgs=list(row.names=FALSE, sep="\t")
)

