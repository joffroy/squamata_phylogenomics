##find out which species are in both datasets mitochondroal and nuclear

nonmt <- read.alignment(file="squamata_refseq_15seqs_SuperAlignment_cs_nonmt.fasta", format="fasta")

mt <- read.alignment(file="squamata_refseq_15seqs_SuperAlignment_cs_mt.fasta", format="fasta")


nam_nonmt<- nonmt$nam
nam_mt<- mt$nam


mtnotsequenced<- nam_nonmt[!(nam_nonmt %in% nam_mt)]
