###filter out protein families that have sequences for less than 15 different species and copy remaining families to a new directory
library(plyr)


allCounts<-read.table(gzfile("silix0709_atleast4_nodup.tsv.gz"),header = TRUE, row.names = 1) 


##turn all counts above 1 to NA
allCounts2<- allCounts
allCounts2[allCounts2>1]<-NA

##turn all 0 to NA
allCounts3<- allCounts2
allCounts3[allCounts2==0]<-NA


##remove columns with only NAs
library(janitor)
allCounts4<-allCounts3
allCounts4<-janitor::remove_empty(allCounts4, which = "cols")             

##remove all protein families that now have sequences for less than 10 species
allCounts5 = allCounts4[,colSums(allCounts4, na.rm = TRUE) >= 10,]


allCounts6 = allCounts4[,colSums(allCounts4, na.rm = TRUE) >= 15,]

write.table(allCounts6,"silix0709_15seq_table.txt",sep="\t",row.names=TRUE)


allCounts7 = allCounts4[,colSums(allCounts4, na.rm = TRUE) >= 14,]





#####silix0709 with at least 10 seqs per family has 5332 proteins
#####silix0709 with at least 15 seqs per family has 768 proteins
####  ""                     14 """                 1469
##--> decreases drastically with between 14 and 15




#### 272 species, 4006 proteins 
###-> each species has at least 10 proteins and each protein has at least 10 sequences(species) left without paralog species, paralogs were not rally removed but not counted
###later need to remove paralogs from sequences by catching sequence names that appear more than once 

###copy needed files to another folder
##create vector of family names
families_final<- colnames(allCounts6)

##create a list of files file.copy(list.of.files, new.folder)
file<- c()
for (i in families_final){
  file[i] <- paste("refseqs_squamata_",i,".fasta", sep = "")
}

##copy files to another folder
setwd ("C:/Users")
file.copy(file, "C:/Users")

