#! /usr/bin/python

# Created on 23/05/12 by jdutheil
# Extract all genes from the GenBank file

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import sys
import re
gb_file= sys.argv[1]
outFile = sys.argv[2]

  
output= open(outFile,'w') 
        
sequences = []
for gb_record in SeqIO.parse(open(gb_file,"r"), "genbank") :
    print("Name %s, %i features" % (gb_record.name, len(gb_record.features)))
    # List all features:
    for gb_feature in gb_record.features :
        if gb_feature.type == 'CDS' :
            if 'translation' in gb_feature.qualifiers :
                if 'product' in gb_feature.qualifiers :
                    id = gb_record.annotations['source'].replace(' ', '_') + "|" + gb_record.name + "|" + gb_feature.qualifiers['protein_id'][0] + "|" + gb_feature.qualifiers['product'][0].replace(' ', '-').replace(':', '-').replace(',', '').replace('(', '').replace(')', '').replace('+', '') 
                    seq = SeqRecord(gb_feature.extract(gb_record.seq), id=id, description="")
                    sequences.append(seq)
                else :
                    if 'gene' in gb_feature.qualifiers :
                        id = gb_record.annotations['source'].replace(' ', '_') + "|" + gb_record.name + "|" + gb_feature.qualifiers['protein_id'][0] + "|" + gb_feature.qualifiers['gene'][0].replace(' ', '-').replace(':', '-').replace(',', '').replace('(', '').replace(')', '').replace('+', '')
                        seq = SeqRecord(gb_feature.extract(gb_record.seq), id=id, description="")
                        sequences.append(seq)
                    else :
                        print("Feature %s has no product and was skipped." % repr(gb_feature))
            else :
                print("Feature %s has no translation and was skipped." % repr(gb_feature))
        #else :
        #    print "Feature %s skipped." % (gb_feature.type)

SeqIO.write(sequences, output, "fasta")
output.close()
