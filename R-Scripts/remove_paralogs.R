###Remove paralogs from remaining protein families


tabseq15<-read.delim("silix0709_15seq_table.txt", header=TRUE)


fam_names<- colnames(tabseq15)

nquote_fam_names<-noquote(fam_names)

try<-as.factor(fam_names)

write.table(fam_names ,file="AllFamilies.txt", append = FALSE, sep = "", row.names = FALSE, col.names = FALSE, quote = FALSE)


library(seqinr)

temp = list.files(pattern="*.fasta")


Personalized_Reader <- function(lambda){
  read.fasta(file=lambda, as.string = TRUE, seqtype = "AA")}



myfiles <- lapply(temp, Personalized_Reader)

nams<- temp
names(myfiles)<- nams


tab<-tabseq15
tab[is.na(tabseq15)]<-0

###create vector of species list for every pritein family

spec_lists<-list()
for (i in 1:ncol(tab)){
  spec_lists[[i]]<- rownames(tab)[tab[i]== max(tab[i])]
}
nams<- temp
names(spec_lists)<- nams

test2<-myfiles$refseqs_squamata_FAM000063_renamed.fasta[names(myfiles$refseqs_squamata_FAM000063_renamed.fasta) %in% spec_lists$refseqs_squamata_FAM000063_renamed.fasta]

new_files<-list()
for (j in 1:length(myfiles)){
  new_files[[j]]<-subset(myfiles[[j]],names(myfiles[[j]]) %in% spec_lists[[j]])
}

nams<- temp
names(new_files)<- nams


for (y in names(new_files)){
  write.fasta(sequences = new_files[[y]], names=names(new_files[[y]]), file.out = paste(y, "_final.fasta", sep=""), open="w", as.string = TRUE)
}




