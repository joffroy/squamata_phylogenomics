###count, sort and filter protein families

prot_fam<-read.delim("refseq_fnodes_named.txt", header=TRUE)

library(plyr)
counted<- ddply(prot_fam, .var = "Family", nrow)


atleast10<-table(counted$V1>9)

Counted2<-subset(counted, V1 >= 10)

library(plyr)
 
nodup <- ddply(prot_fam, .var = "Family", .fun = function(d) { t <- table(d$Species_name); return(d[t == 1,]) })

counted<- ddply(nodup, .var = "Family", nrow)
counted2<-subset(counted, V1 >= 10)                

nodup2 <- subset(nodup, Family %in% counted2$Family)

counts <- dlply(prot_fam, .var = "Family", .fun =function(d) { t <- table(d$Species_name); df <- as.data.frame(t); names(df) <- c("Taxon", unique(d$Family)); return(df) })

require(dplyr)
allCounts <- counts[[1]]
pb <- txtProgressBar(0, length(counts) - 1, style = 3)
for (i in 2:length(counts)) {
  setTxtProgressBar(pb, i - 1)
  allCounts <- full_join(allCounts, counts[[i]], by = "Taxon")
}

write.table(allCounts, file=gzfile("GeneCounts.tsv.gz"), sep = "\t", row.names = FALSE)


