Use the SGEDTools: https://github.com/jydu/sgedtools

Create a structure index:

```bash
python3.9 sged-create-structure-index.py \
         --pdb=*.cif \
         --pdb-format=mmCIF \
         --alignment=PLA2_alignment_branch_site6_aa.fasta \
         --alignment-format=fasta \
         --output=PLA2_PdbIndex.txt
```

