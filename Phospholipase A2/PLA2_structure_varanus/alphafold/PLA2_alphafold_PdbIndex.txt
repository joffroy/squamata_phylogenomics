# SGED index file version 0.99
# SGED input alignment = PLA2_alignment_branch_site6_aa.fasta
# SGED input alignment sequence = Varanus_komodoensis
# SGED input PDB = selected_prediction_full.pdb
# SGED input PDB chain = A
# SGED index start
AlnPos,PdbRes
1,NA
2,ALA15
3,PHE22
4,GLN29
5,GLY30
6,PRO31
7,ASP32
8,SER33
9,CYS34
10,CYS35
11,ARG36
12,GLU37
13,HIS38
14,ASP39
15,GLN40
16,CYS41
17,SER42
18,VAL43
19,GLN44
20,ILE45
21,THR46
22,ALA47
23,LEU48
24,GLN49
25,ARG50
26,LYS51
27,HIS52
28,GLY53
29,ILE54
30,PHE55
31,ASN56
32,LEU57
33,ARG58
34,PRO59
35,TYR60
36,THR61
37,ILE62
38,SER63
39,HIS64
40,CYS65
41,ASP66
42,CYS67
43,ASP68
44,THR69
45,ARG70
46,PHE71
47,ARG72
48,LEU75
49,MET76
50,ASP77
51,LEU78
52,ASN79
53,ASP80
54,THR81
55,ILE82
56,ALA83
57,ASP84
58,PHE85
59,ILE86
60,GLY87
61,THR88
62,THR89
63,TYR90
64,PHE91
65,SER92
66,VAL93
67,LEU94
68,GLN95
69,ILE96
70,PRO97
71,CYS98
72,PHE99
73,TYR100
74,LEU101
75,GLU102
76,GLU103
77,SER104
78,GLU106
79,ALA107
80,CYS108
81,LEU109
82,GLU110
83,TRP111
84,ALA112
85,TRP113
86,TRP114
87,CYS117
88,ASN118
89,LYS119
90,ARG120
91,GLY121
92,ARG122
93,MET123
94,LEU124
95,MET125
96,ALA126
97,HIS127
98,THR128
99,VAL129
100,PRO130
101,PRO131
102,SER132
103,ALA133
104,TYR134
105,GLY135
