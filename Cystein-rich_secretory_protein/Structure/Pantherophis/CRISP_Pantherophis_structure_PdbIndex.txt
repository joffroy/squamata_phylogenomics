# SGED index file version 0.99
# SGED input alignment = CRISP_subset_PAML_BSM10_prot.fasta
# SGED input alignment sequence = Pantherophis_guttatus|XM_034404849
# SGED input PDB = full_model_309284_1.pdb
# SGED input PDB chain = A
# SGED index start
AlnPos,PdbRes
1,NA
2,NA
3,VAL13
4,LEU14
5,GLN15
6,GLN16
7,SER17
8,GLN34
9,LYS35
10,GLU36
11,ILE37
12,VAL38
13,ASP39
14,LYS40
15,HIS41
16,ASN42
17,ALA43
18,LEU44
19,ARG45
20,ARG46
21,SER47
22,VAL48
23,ARG49
24,PRO50
25,THR51
26,ALA52
27,ARG53
28,ASN54
29,MET55
30,LEU56
31,GLN57
32,MET58
33,GLU59
34,TRP60
35,ASN61
36,SER62
37,ASN63
38,ALA64
39,THR65
40,GLN66
41,ASN67
42,ALA68
43,LYS69
44,ARG70
45,TRP71
46,ALA72
47,ASP73
48,ARG74
49,CYS75
50,HIS79
51,SER80
52,PRO81
53,GLN82
54,HIS83
55,LEU84
56,ARG85
57,LYS89
58,LEU90
59,SER91
60,CYS92
61,GLY93
62,GLU94
63,ASN95
64,LEU96
65,PHE97
66,MET98
67,SER99
68,SER100
69,HIS101
70,PRO102
71,HIS103
72,SER104
73,TRP105
74,THR106
75,ARG107
76,VAL108
77,ILE109
78,GLN110
79,ALA111
80,TRP112
81,TYR113
82,ASN114
83,GLU115
84,TYR116
85,LYS117
86,LYS118
87,PHE119
88,ARG120
89,TYR121
90,GLY122
91,VAL123
92,GLY124
93,ALA125
94,ASN126
95,PRO127
96,PRO128
97,GLY129
98,SER130
99,VAL131
100,TYR135
101,THR136
102,GLN137
103,ILE138
104,VAL139
105,TRP140
106,TYR141
107,LYS142
108,SER143
109,HIS144
110,LEU145
111,VAL146
112,GLY147
113,CYS148
114,ALA149
115,ALA150
116,ALA151
117,ARG152
118,CYS153
119,PHE158
120,TYR160
121,VAL163
122,CYS164
123,HIS165
124,TYR166
125,CYS167
126,PRO168
127,ALA169
128,GLY170
129,ASN171
130,ILE172
131,VAL173
132,GLY174
133,SER175
134,ILE176
135,ALA177
136,THR178
137,PRO179
138,TYR180
139,LYS181
140,SER182
141,GLY183
142,PRO184
143,PRO185
144,CYS186
145,GLY187
146,ASP188
147,CYS189
148,PRO190
149,SER191
150,ALA192
151,CYS193
152,VAL194
153,ASN195
154,GLY196
155,LEU197
156,CYS198
157,CYS202
158,GLU203
159,TYR204
160,VAL205
161,ASN206
162,ASN210
163,CYS211
164,GLU212
165,ALA213
166,LEU214
167,CYS220
168,PRO230
169,ALA231
170,SER232
171,CYS233
172,PHE234
173,CYS235
